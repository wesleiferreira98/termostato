/* ***************************************************************
* Autor............: Weslei Ferreira Santos
* Inicio...........: 14/07/2021
* Ultima alteracao.: 04/06/2022
* Nome.............: temostato
* Funcao...........: É um programa feito para Simular o funcionamento de um ventilador
*************************************************************** */

#include <SPI.h>//lib para o cartao sd
#include <SD.h>//lib para o cartao sd
#include <DHT_U.h>//lib para o sensor de temperatura
#include <DHT.h>//lib para o sensor de temperatura
#include <EEPROM.h>//lib para a memoria EEPROM
#include <LiquidCrystal_I2C.h>//lib para o display I2C
#include <IRremote.h>//lib para o controle remoto

// pinos dos sensores e motores
#define pinSen 8
#define DHTTYPE DHT11
#define sensorLuz 7
#define velmotor 3
#define m1a 4
#define m1b 5

// objetos de display e sensor de temperatura e cartão de memoria
LiquidCrystal_I2C lcd(0x27,16, 2);
DHT sensor(pinSen,DHTTYPE);
File TemperaturaSalva;

//variaveis e constantes para controle do motor e display
const byte IR_RECEIVE_PIN = 2;
unsigned long previousMillis = 0;
const long intervalo = 10000;
unsigned long currentMillis = millis();
float temp=30.0;
int endereco=0;

// prototipagem das funções de controle
void setLCD();
void setTemp();
void setMotor(float tmp);
void DesMotor();
void gravaTemperatura(float tmp);
float retTemperatura();

void setup() {
  lcd.init();
  pinMode(sensorLuz,INPUT);
  pinMode(m1a,OUTPUT);
  sensor.begin();
  float h = sensor.readHumidity();
  float t = sensor.readTemperature();
  IrReceiver.begin(2, ENABLE_LED_FEEDBACK);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Iniciando da memoria de persistência...");
  

  if (!SD.begin(4)) {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("falha na inicialização :(");
    
  }else{
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Inciação completa :)");
     setMotor(retTemperatura());
  }
  

  

}

void loop() {
  // put your main code here, to run repeatedly:
  
  int info=digitalRead(sensorLuz);
  if(info==1){
    lcd.backlight(); 
  }else{
    lcd.noBacklight(); 
  }
  if (IrReceiver.decode())
   {
      int cod=IrReceiver.decodedIRData.command;
      
      switch(cod){
        case 22:
          setLCD();
          
        break;
        case 82:
          temp=temp-10;
          setTemp();
          gravaTemperatura(temp);
          setMotor(retTemperatura());
        break;
        case 24:
          temp=temp+10;
          gravaTemperatura(temp);
          setMotor(retTemperatura());
        break;
        case 25:
          DesMotor();
        break;
        case 13:
          DesMotor();
        break;
        
          
      }
      IrReceiver.resume();
   }
  

}
/* ***************************************************************
   * Metodo: setLCD
   * Funcao: Imprime no display a temperatura atual
   * Parametros: nao recebe parametros
   * Retorno: nao retorna valores
   *************************************************************** */
void setLCD(){

  float h = sensor.readHumidity();
  float t = sensor.readTemperature();

  lcd.setCursor(0,0);
  lcd.print("TEMP");
  lcd.setCursor(5,0);
  lcd.print(t);
  lcd.setCursor(0,1);
  lcd.print("HUMI");
  lcd.setCursor(5,1);
  lcd.print(h);

  if (currentMillis - previousMillis >= intervalo){
    previousMillis = currentMillis;
    lcd.clear();
    delay(700);
  }
}
/* ***************************************************************
   * Metodo: setLCD
   * Funcao: Imprime no display o valor da temperatura passado pelo usuario
   * Parametros: nao recebe parametros
   * Retorno: nao retorna valores
   *************************************************************** */
void setTemp(){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("NOV TEMP");
  lcd.setCursor(9,0);
  lcd.print(temp);

}
/* ***************************************************************
   * Metodo: setMotor
   * Funcao: liga o motor de acordo a temperatura ambiente
   * Parametros: recebe parametros do tipo float
   * Retorno: nao retorna valores
   *************************************************************** */

void setMotor(float tmp){
  if(sensor.readTemperature()>=tmp){
    digitalWrite(m1a,HIGH);
    
  }else{
    digitalWrite(m1a,LOW);
  }
}
/* ***************************************************************
   * Metodo: DesMotor
   * Funcao: liga/desliga o motor de acordo a vontade do usuario
   * Parametros: não recebe parametros 
   * Retorno: nao retorna valores
   *************************************************************** */
void DesMotor(){
  static bool est=true;
  if(est==true){
    digitalWrite(m1a,LOW);
    est=!est;
  }else{
    digitalWrite(m1a,HIGH);
    est=!est;
  }
}
/* ***************************************************************
   * Metodo: gravaTemperatura
   * Funcao: grava a temperatura fonecida pelo o usuario no cartao de memoria,
   * caso ocorra erros essa informação será gravada na memoria EEPROM
   * Parametros: recebe parametros do tipo float
   * Retorno: nao retorna valores
   *************************************************************** */
void gravaTemperatura(float tmp){
  TemperaturaSalva = SD.open("temp.txt",FILE_WRITE);
  if(TemperaturaSalva){
    TemperaturaSalva.print(tmp);
    TemperaturaSalva.close();
    
  }else{
    EEPROM.write(endereco,tmp);
  }
  
  
  
}
/* ***************************************************************
   * Metodo: retTemperatura
   * Funcao: retorna a temperatura armazenada no cartao SD, caso ocorra
   * erros será retornado o valor padrão
   * Parametros: recebe parametros do tipo float
   * Retorno: nao retorna valores
   *************************************************************** */
float retTemperatura(){
  float temp;
  TemperaturaSalva = SD.open("temp.txt");
  if(TemperaturaSalva){
    while(TemperaturaSalva.available()){
      temp = (float) TemperaturaSalva.read();
    }
  TemperaturaSalva.close();
  
  }else{
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Erro ao obert temp");
     temp=(float)EEPROM.read(endereco);
    
  }
  return temp;
  
}
